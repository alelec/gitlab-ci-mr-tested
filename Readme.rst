Gitlab CI Merge Request Tested?
===============================

We use informal testing alongside automated CI testing which is documented in our
merge requests.

Once a project member has done some testing, the basic test steps are documented in a
comment on the merge request with the tested label attached to that comment by addding
the text "~tested" to the comment.

This python script is run as the last step of ci and  uses the gitlab api to
inspect Merge Requests covering the current commit to see if they have a "tested" comment.
If not, the "untested" label is added to the MR.
If the tested comment does exist, the "untested" label is removed and the "tested" one added.

If the tested comment is not present, the CI job is failed.


It can be used in a gitlab-ci.yml stage like:

.. code:: yaml

    stages:
      - test
      - build
      - release
      - tested

    < ... snip ... >

    mr_tested:
      only:
        - branches
      stage: tested
      image: python:3
      script:
        - pip install gitlab_ci_mr_tested
        - gitlab_ci_mr_tested --token "${PRIVATE_TOKEN}" --label "tested" --add-untested "untested"
