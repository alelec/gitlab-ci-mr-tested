from setuptools import setup
from os import path

# Get the long description from the README file
with open(path.join(path.dirname(__file__), 'Readme.rst'), 'r') as f:
    long_description = f.read()

setup(
    name='gitlab_ci_mr_tested',
    use_scm_version=True,  # This will generate the version number from git tags

    description='Utility for use in gitlab ci to check merge requests have been marked tested',
    long_description=long_description,
    url='https://gitlab.com/alelec/gitlab-ci-mr-tested',
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    license='MIT',
    py_modules=['gitlab_ci_mr_tested'],
    setup_requires=['setuptools_scm'],
    install_requires=['python-gitlab', 'click'],
    entry_points={
        'console_scripts': [
            'gitlab_ci_mr_tested=gitlab_ci_mr_tested:mr_tested',
        ],
    },
)
